# Use an official Python runtime as the base image
FROM python:3.10

# Set the working directory in the container
WORKDIR /busser

# Copy the project files into the container
COPY . .

# Install the required Python packages
RUN apt-get update && apt-get install -y ffmpeg

# Install PyNaCl library
RUN pip install PyNaCl


RUN apt-get update && apt-get install -y portaudio19-dev
RUN pip install --no-cache-dir -r requirements.txt

# Load environment variables from .env
ENV $(cat .env | xargs)

# Adjust the PYTHONPATH to include the current directory
ENV PYTHONPATH "${PYTHONPATH}:/busser"

# Set the command to run the application
CMD ["python", "src/busser.py"]
