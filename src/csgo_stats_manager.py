import os
import requests
import json
import discord
from dotenv import load_dotenv

load_dotenv()
STEAM_API_KEY = os.getenv('STEAM_API_KEY')


class CounterstrikeStats:
    def __init__(self, steamid):
        self.steamid = steamid
        self.player_stats = {}
        self.last_match_stats = {}

    def download_csgo_stats(self):
        url = f"https://api.steampowered.com/ISteamUserStats/GetUserStatsForGame/v0002/?appid=730&key={STEAM_API_KEY}&steamid={self.steamid}"
        response = requests.get(url)
        if response.status_code == 200:
            data = json.loads(response.text)
            return data
        else:
            print(f"Error: {response.status_code} - {response.text}")
            return None

    def update_player_stats(self, data):
        for stat in data["playerstats"]["stats"]:
            self.player_stats[stat['name']] = stat['value']

    def update_last_match_stats(self, data):
        last_match_stats = {}
        for stat in data["playerstats"]["stats"]:
            if 'last_match' in stat['name']:
                last_match_stats[stat['name']] = stat['value']
        self.last_match_stats = last_match_stats

    def get_player_stats(self):
        return self.player_stats

    def get_last_match_stats(self):
        return self.last_match_stats


def get_steam_id(input_str):
    if input_str.isdigit():
        return input_str
    else:
        vanity_url = extract_vanity_url(input_str)
        if vanity_url:
            url = f"https://api.steampowered.com/ISteamUser/ResolveVanityURL/v0001/?key={STEAM_API_KEY}&vanityurl={vanity_url}"
            response = requests.get(url)
            data = json.loads(response.text)
            if data["response"]["success"] == 1:
                return data["response"]["steamid"]
    return None


def extract_vanity_url(input_str):
    if "steamcommunity.com/id/" in input_str:
        parts = input_str.split("steamcommunity.com/id/")
        if len(parts) > 1:
            vanity_url = parts[1].split("/")[0]
            return vanity_url.strip()
    return None


async def get_csgo_stats(ctx, input_str):
    steamid = get_steam_id(input_str)
    if steamid:
        player = CounterstrikeStats(steamid)
        data = player.download_csgo_stats()
        if data:
            player.update_player_stats(data)
            player_stats = player.get_player_stats()
            await send_csgo_stats_embed(ctx, player_stats, input_str, "Life Time")
        else:
            await ctx.send("Failed to retrieve CSGO stats. Please try again later or contact the Bus")
    else:
        await ctx.send("Invalid Steam ID or vanity URL provided.")


async def send_csgo_stats_embed(ctx, player_stats, input_str, time_frame):
    match_stats = format_csgo_stats(player_stats, time_frame)
    embed = discord.Embed(title=f"{time_frame} Stats")
    embed.add_field(name=f"{time_frame} Stats For {input_str}", inline=True, value=match_stats)
    await ctx.send(embed=embed)


def format_csgo_stats(player_stats, time_frame):
    match_stats = "\u200b"

    def get_stat(key, default="N/A"):
        return str(player_stats.get(key, default))

    last_match_stats = (
        f"**Last Match Stats:**\n"
        f"K/D: {get_stat('last_match_kills')}/{get_stat('last_match_deaths')}\n"
        f"MVPs: {get_stat('last_match_mvps')}\n"
        f"Damage: {get_stat('last_match_damage')}\n"
        f"Money Spent: {get_stat('last_match_money_spent')}\n"
    )

    total_kills_stats = (
        f"**Total Kills Stats:**\n"
        f"Headshot Kills: {get_stat('total_kills_headshot')}\n"
        f"Grenade Kills: {get_stat('total_kills_hegrenade')}\n"
        f"Knife Kills: {get_stat('total_kills_knife')}\n"
        f"Deagle Kills: {get_stat('total_kills_deagle')}\n"
    )

    bomb_stats = (
        f"**Bomb Stats:**\n"
        f"Bombs Planted: {get_stat('total_planted_bombs')}\n"
        f"Bombs Defused: {get_stat('total_defused_bombs')}\n"
    )

    if time_frame == "Last Match":
        match_stats += last_match_stats
    else:
        match_stats += (
            f"{total_kills_stats}"
            f"{bomb_stats}"
        )

    return match_stats