import os
import random
import discord
from discord.ext import commands
from dotenv import load_dotenv
import logging

from src.audio_player import AudioPlayer
from csgo_stats_manager import get_csgo_stats
from crypto_manager import CryptoManager
from src.osrs import OSRSManager

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')

load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')
GUILD = os.getenv('DISCORD_GUILD')


def get_prefix(bot, message):
    prefixes = ["bus ", "busser ", "\\", "bot "]
    return commands.when_mentioned_or(*[prefix.lower() for prefix in prefixes])(bot, message)


intents = discord.Intents.all()
bot = commands.Bot(command_prefix=get_prefix, intents=intents, case_insensitive=True)
audio_player = AudioPlayer(bot)
crypto_manager = CryptoManager()
osrs_manager = OSRSManager()


# Start
@bot.event
async def on_ready():
    logging.info(f"{bot.user.name} v2 is now online.")


# MARKETS
@bot.command(pass_context=True,
             brief="[Market] Lists current prices of popular cryptos")
async def crypto(ctx):
    prices = crypto_manager.get_live_crypto_prices()
    message = crypto_manager.display_crypto_prices(prices)
    await ctx.send(message)


# MUSIC
@bot.command(name="play",
             brief="[Music] Play a song or audio file",
             help='Usage: busser play <YouTube URL or Query>',
             arguments='YouTube URL/Query')
async def play(ctx, *, query):
    await audio_player.play(ctx, query)


@bot.command(name="pause",
             brief="[Music] Pause the current song")
async def pause(ctx):
    await audio_player.pause(ctx)


@bot.command(name="resume",
             brief="[Music] Resume the paused song")
async def resume(ctx):
    await audio_player.resume(ctx)


@bot.command(pass_context=True,
             brief="[Music] Skips current audio being played")
async def skip(ctx):
    await audio_player.skip(ctx)


@bot.command(pass_context=True,
             brief="[Music] Stops the audio & disconnects")
async def stop(ctx):
    await audio_player.stop(ctx)


@bot.command(pass_context=True, aliases=['disconnect'],
             brief="[Music] Disconnects from DCVC")
async def leave(ctx):
    await audio_player.leave(ctx)


@bot.command(pass_context=True,
             brief="[Music] Joins the DCVC")
async def join(ctx):
    await audio_player.join(ctx)


@bot.command(name="autoplay", brief="[Music] Enable or disable auto-play")
async def autoplay(ctx, enable: bool):
    audio_player.auto_play = enable
    await ctx.send(f"Auto-play has been {'enabled' if enable else 'disabled'}.")


# Gaming
@bot.command(pass_context=True,
             brief="[Gaming] Returns all time CSGO stats [More coming soon]")
async def csgo(ctx, steamid_or_vanity):
    await get_csgo_stats(ctx, steamid_or_vanity)


@bot.command(pass_context=True, brief="[Gaming] Returns OSRS stats for a player")
async def osrs(ctx, username, duration="", skill=""):
    stats_embed = await osrs_manager.get_osrs_stats(username, duration, skill)
    if stats_embed is None:
        await ctx.send(f"No stats found for {username} in the specified duration.")
    else:
        await ctx.send(embed=stats_embed)

    boss_embed = await osrs_manager.get_osrs_bosses(username, duration)
    if boss_embed is not None:
        await ctx.send(embed=boss_embed)


# MISC
@bot.command(brief="[Misc] A 50/50 shot at getting heads or tails",
             help=f"busser flip")
async def flip(ctx):
    await ctx.send("Heads") if random.randint(0, 1) == 0 else await ctx.send("Tails")


@bot.command(brief="[Misc] A 50/50 shot at getting CT (heads) or T (tails)")
async def csflip(ctx):
    ct_logo = 'https://static.wikia.nocookie.net/cswikia/images/4/4c/Csgo_CT_icon_alt.png/revision/latest/scale-to' \
              '-width-down/180?cb=20151222191721 '
    t_logo = "https://static.wikia.nocookie.net/cswikia/images/2/25/Csgo_Terror_icon_alt.png/revision/latest/scale-to" \
             "-width-down/180?cb=20151222191720 "
    await ctx.send(ct_logo) if random.randint(0, 1) == 0 else await ctx.send(t_logo)


@bot.command(name="gnf", brief="[Misc] Goodnight, forever.")
async def gnf(ctx):
    await ctx.send("Goodnight, forever.")


@bot.command(name="commands", aliases=['command'], brief="[Misc] Display a list of available commands")
async def show_commands(ctx):
    command_list = get_command_list()
    await ctx.send(f"**Available Commands:**\n{command_list}")


def get_command_list():
    command_list = []
    for command in bot.commands:
        command_list.append(f"`!{command.name}`: {command.brief}")
    return "\n".join(command_list)


bot.run(TOKEN)