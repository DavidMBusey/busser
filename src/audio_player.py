import asyncio
import os
import re

import yt_dlp
from discord import FFmpegPCMAudio
from discord import PCMVolumeTransformer
from googleapiclient.discovery import build
from dotenv import load_dotenv
from googleapiclient.errors import HttpError
from youtubesearchpython import VideosSearch

load_dotenv()
YT_API_KEY = os.getenv('YOUTUBE_API_KEY')

class AudioPlayer:
    def __init__(self, bot):
        self.bot = bot
        self.queues = {}
        self.now_playing = {}
        self.last_played_video = {}
        self.auto_play = False
        self.YDL_OPTS = {
            'format': 'bestaudio',
            'noplaylist': True,
            'postprocessors': [{
                'key': 'FFmpegExtractAudio',
                'preferredcodec': 'mp3',
                'preferredquality': '192',
            }],
        }

    async def play(self, ctx, query):
        voice_client = ctx.voice_client
        if voice_client is None:
            voice_client = await ctx.author.voice.channel.connect()

        # Create a queue for the guild if it doesn't exist
        if ctx.guild.id not in self.queues:
            self.queues[ctx.guild.id] = []

        # Check if the query is a YouTube URL or a search query
        if "youtube.com" in query or "youtu.be" in query:
            with yt_dlp.YoutubeDL(self.YDL_OPTS) as ydl:
                video_info = ydl.extract_info(query, download=False)
                if 'entries' in video_info:
                    video_info = video_info['entries'][0]
                video = Video(video_info, ctx.author)
                self.queues[ctx.guild.id].append(video)
        else:
            # Extract the quoted part of the query (if present)
            quoted_query = re.search(r'"(.+?)"', query)
            if quoted_query:
                search_query = quoted_query.group(1)
            else:
                search_query = query

            # Search YouTube for the query
            videos_search = VideosSearch(search_query, limit=5)
            videos = videos_search.result()
            if not videos:
                await ctx.send("No videos found for the given search query.")
                return

            # Send the top 5 results to the user
            result_message = "Please select a video to play (1-5):\n"
            for i, video in enumerate(videos["result"], start=1):
                result_message += f"{i}. {video['title']}\n"
            await ctx.send(result_message)

            # Wait for the user's input
            def check(m):
                return m.author == ctx.author and m.content.isdigit() and 1 <= int(m.content) <= 5
            try:
                user_choice = await self.bot.wait_for("message", check=check, timeout=30.0)
            except asyncio.TimeoutError:
                await ctx.send("You took too long to respond. Please try again.")
                return

            # Get the chosen video URL and add it to the queue
            chosen_video = videos["result"][int(user_choice.content) - 1]
            video_url = f"https://www.youtube.com/watch?v={chosen_video['id']}"
            with yt_dlp.YoutubeDL(self.YDL_OPTS) as ydl:
                video_info = ydl.extract_info(video_url, download=False)
                if 'entries' in video_info:
                    video_info = video_info['entries'][0]
                video = Video(video_info, ctx.author)
                self.queues[ctx.guild.id].append(video)

        if not voice_client.is_playing():
            await self.play_next(ctx)

    async def play_next(self, ctx):
        if not self.queues[ctx.guild.id]:
            if self.auto_play:
                recommended_video_url = await self.get_recommended_video(self.last_played_video.get(ctx.guild.id))
                if recommended_video_url:
                    with yt_dlp.YoutubeDL(self.YDL_OPTS) as ydl:
                        video_info = ydl.extract_info(recommended_video_url, download=False)
                        if 'entries' in video_info:
                            video_info = video_info['entries'][0]
                        video = Video(video_info, ctx.author)
                        self.queues[ctx.guild.id].append(video)
                else:
                    await ctx.send("Queue is empty.")
                    return
            else:
                await ctx.send("Queue is empty.")
                return

        video = self.queues[ctx.guild.id].pop(0)
        self.now_playing[ctx.guild.id] = video
        self.last_played_video[ctx.guild.id] = video

        audio_source = FFmpegPCMAudio(video.stream_url)
        audio_source = PCMVolumeTransformer(audio_source)
        ctx.voice_client.play(audio_source,
                              after=lambda e: asyncio.run_coroutine_threadsafe(self.play_next(ctx), self.bot.loop))

        await ctx.send(f"Now playing: {video.title} (requested by {video.requested_by.mention})")

    async def get_recommended_video(self, last_played_video):
        if not last_played_video:
            print("No last played video provided.")
            return None

        video_title = last_played_video.title

        print(f"Getting recommended video for video title: {video_title}")

        try:
            youtube = build('youtube', 'v3', developerKey=YT_API_KEY)

            request = youtube.search().list(
                part='snippet',
                q=video_title,
                type='video',
                maxResults=1
            )
            response = request.execute()

            if 'items' in response and response['items']:
                recommended_video_id = response['items'][0]['id']['videoId']
                print(f"Recommended video ID: {recommended_video_id}")
                return f"https://www.youtube.com/watch?v={recommended_video_id}"
            else:
                print(f"No recommended video found for video title: {video_title}")
                return None
        except HttpError as e:
            print(f"HTTP error occurred: {e}")
            print(f"HTTP error details: {e.error_details}")
            return None
        except Exception as e:
            print(f"Exception occurred while getting recommended video: {e}")
            print(f"Exception details: {type(e).__name__}: {str(e)}")
            return None

    async def pause(self, ctx):
        ctx.voice_client.pause()
        await ctx.send("Audio paused.")

    async def resume(self, ctx):
        ctx.voice_client.resume()
        await ctx.send("Audio resumed.")

    async def skip(self, ctx):
        if not ctx.voice_client.is_playing():
            await ctx.send("No song is currently playing.")
            return
        ctx.voice_client.stop()
        if self.queues[ctx.guild.id]:
            await self.play_next(ctx)
        else:
            await ctx.send("Skipped the current song. No more songs in the queue.")

    async def stop(self, ctx):
        if not ctx.voice_client or not ctx.voice_client.is_connected():
            await ctx.send("I am not currently connected to a voice channel.")
            return
        ctx.voice_client.stop()
        self.queues[ctx.guild.id] = []
        self.now_playing[ctx.guild.id] = None
        self.last_played_video[ctx.guild.id] = None
        await ctx.voice_client.disconnect()
        await ctx.send("Audio stopped and disconnected.")

    async def leave(self, ctx):
        if not ctx.voice_client or not ctx.voice_client.is_connected():
            await ctx.send("I am not currently connected to a voice channel.")
            return
        self.queues[ctx.guild.id] = []
        self.now_playing[ctx.guild.id] = None
        self.last_played_video[ctx.guild.id] = None
        await ctx.voice_client.disconnect()
        await ctx.send("Disconnected from voice channel.")
    async def join(self, ctx):
        if ctx.author.voice:
            await ctx.author.voice.channel.connect()
            await ctx.send("Connected to voice channel.")
        else:
            await ctx.send("You must be in a voice channel to use this command.")


class Video:
    def __init__(self, info_dict, requested_by):
        self.info_dict = info_dict
        self.title = info_dict.get('title')
        self.url = info_dict.get('webpage_url')
        self.stream_url = info_dict.get('url')
        self.thumbnail = info_dict.get('thumbnail')
        self.requested_by = requested_by