import discord
import requests
from bs4 import BeautifulSoup
from osrs_data import SKILL_ICONS, BOSS_ICONS


class OSRSManager:
    def __init__(self):
        self.remove_zero_exp = True

    async def get_osrs_stats(self, username, duration, skill):
        _var1 = f"https://templeosrs.com/php/add_datapoint.php?player={username}"
        requests.get(_var1)

        duration_mapping = {
            "day": "1day",
            "week": "7days",
            "month": "31days",
            "6months": "186days",
            "year": "365days",
            "all": "alltime"
        }

        duration = duration_mapping.get(duration.lower(), "7days")

        URL = f"https://templeosrs.com/player/overview.php?player={username}&duration={duration}&skill={skill}"
        page = requests.get(URL).text
        soup = BeautifulSoup(page, "lxml")
        total_high_scores = soup.find("table", attrs={"class": "total-tracking-table"})

        duration_text = "7 days (default)" if duration == "7days" else duration
        embed = discord.Embed(title=f"{username} OSRS Hiscores",
                              description=f"Duration: {duration_text}, Skill: {skill}")

        image_counter = 0
        stats_columns = ["\u200b"] * 3

        for hs in total_high_scores.find_all("tr"):
            skills = hs.find_all(class_="tracking-skill")
            xps = hs.find_all(class_="tracking-xp")
            ehps = hs.find_all(class_="tracking-ehp")

            for s in skills:
                img = SKILL_ICONS[image_counter]
                image_counter += 1
                skill_name = s.find('a').text

            xp = next((str(_xp.text) for _xp in xps), '')
            if self.remove_zero_exp and (xp == '0' or xp == '0.00' or xp == ''):
                continue

            ehp_text = next((str(f"({ehp.text})").rjust(10) for ehp in ehps), '')
            xp_and_ehp = f'{xp:<2} {ehp_text:>}'

            column_index = min(image_counter // 9, 2)
            stats_columns[column_index] += f"{img}{xp_and_ehp}\n"

        if all(col == "\u200b" for col in stats_columns):
            return None

        for i, col in enumerate(stats_columns):
            embed.add_field(name=f"Experience (EHP) - Column {i + 1}", inline=True, value=col)

        return embed

    async def get_osrs_bosses(self, username, duration):
        duration_mapping = {
            "day": "1day",
            "week": "7days",
            "month": "31days",
            "6months": "186days",
            "year": "365days",
            "all": "alltime"
        }

        duration = duration_mapping.get(duration.lower(), "7days")

        URL = f"https://templeosrs.com/player/overviewpvm.php?player={username}&duration={duration}"
        page = requests.get(URL).text
        soup = BeautifulSoup(page, "lxml")
        total_high_scores = soup.find("table", attrs={"class": "total-tracking-table"})

        image_counter = 0
        boss_column = "\u200b"

        for hs in total_high_scores.find_all("tr")[1:]:
            bosses = hs.find_all(class_="tracking-skill")
            kills = hs.find_all(class_="tracking-xp")
            ehps = hs.find_all(class_="tracking-ehp")

            boss_name = next((boss.find('a').text for boss in bosses), '')
            kills_text = next((kill.text for kill in kills), '')
            ehp_text = next((ehp.text for ehp in ehps), '')

            if kills_text == '0' or ehp_text == '0':
                image_counter += 1
                continue

            boss_icon = BOSS_ICONS[min(image_counter, len(BOSS_ICONS) - 1)]
            boss_column += f"{boss_name}{boss_icon}: {kills_text} ({ehp_text})\n"
            image_counter += 1

        if boss_column == "\u200b":
            return None

        embed = discord.Embed(title=f"{username} OSRS Boss Hiscores", description=f"Duration: {duration}")
        embed.add_field(name="Boss Kills (EHP)", inline=True, value=boss_column)

        return embed