SKILL_ICONS = [
    "<:overall:901493522950148136>", "<:attack:901493522522325094>", "<:defense:901497005858226206>",
    "<:skill_icon_Strength:901493523013058650>", "<:hitpoints:901493522904010752>", "<:range:901493523017240587>",
    "<:prayer:901493522983690360>", "<:mgaic:901493522954321920>", "<:cooking:901493522924961792>",
    "<:wc:901493523092734013>", "<:skill_icon_Fletching:901497674799411220>", "<:fishing:901493522933366814>",
    "<:firemaking:901493522706866197>", "<:crafting:901493522618777631>", "<:smithing:901493522975301752>",
    "<:mining:901493522929180692>", "<:herblore:901493522924974111>", "<:agility:901493522933354526>",
    "<:thieving:901493522983686164>", "<:slayer:901493522803327017>", "<:farming:901493522958528622>",
    "<:rc:901493522983714816>", "<:hunter:901493522706874389>", "<:construction:901493522916605974>",
    "<:ehp:901493522899808276>"
]

BOSS_ICONS = [
    "<:abyssal_sire:902447873273262120>", "<:alchemical_hydra:902447873143238667>", "<:callisto:902447873239711804>",
    "<:cerberus:902447872925122571>", "<:chambers_of_xeric:902447873344536597>",
    "<:chambers_of_xeric_challenge_mode:902447872920928258>", "<:chaos_elemental:902447873352957953>",
    "<:chaos_fanatic:902447873189351465>", "<:commander_zilyana:902447873269063700>",
    "<:corporeal_beast:902447872996433941>", "<:dagannoth_prime:902447873294221312>",
    "<:dagannoth_rex:902447873331982356>", "<:dagannoth_supreme:902447873344557066>",
    "<:general_graardor:902447873189380137>", "<:giant_mole:902447873248075777>",
    "<:grotesque_guardians:902447873277456384>", "<:kalphite_queen:902447873281630208>",
    "<:king_black_dragon:902447873281622096>", "<:kraken:902447873260662794>", "<:kreearra:902447872988037181>",
    "<:kril_tsutsaroth:902447873290043433>", "<:nightmare:902447873273262110>", "<:sarachnis:902447873344548886>",
    "<:scorpia:902447873063530508>", "<:skotizo:902447873281654784>", "<:the_gauntlet:902447873269051453>",
    "<:the_corrupted_gauntlet:902658086341341294>", "<:theatre_of_blood:902447873361342464>",
    "<:thermonuclear_smoke_devil:902447873352929280>", "<:tzkal_zuk:902447873495535616>",
    "<:tztok_jad:902447873248075778>", "<:venenatis:902447873331957770>", "<:vetion:902447873382293534>",
    "<:vorkath:902447873336176670>", "<:zulrah:902447873294209056>", "<:theatre_dust:902447873008996393>",
    "<:phosanis:902447873269039154>", "<:ehp:901493522899808276>"
]

SKILLS = [
    'Overall', 'Attack', 'Defence', 'Strength', 'Hitpoints', 'Ranged', 'Prayer', 'Magic',
    'Cooking', 'Woodcutting', 'Fletching', 'Fishing', 'Firemaking', 'Crafting', 'Smithing',
    'Mining', 'Herblore', 'Agility', 'Thieving', 'Slayer', 'Farming', 'Runecraft', 'Hunter',
    'Construction'
]

BOUNTY_HUNTERS = ['Bounty Hunter - Hunter', 'Bounty Hunter - Rogue']

CLUE_SCROLLS = [
    'Clue Scrolls (All)', 'Clue Scrolls (Beginner)', 'Clue Scrolls (Easy)',
    'Clue Scrolls (Medium)', 'Clue Scrolls (Hard)', 'Clue Scrolls (Elite)',
    'Clue Scrolls (Master)'
]

MINIGAMES = [
    'LMS - Rank', 'Soul Wars Zeal', 'Abyssal Sire', 'Alchemical Hydra', 'Barrows Chests',
    'Bryophyta', 'Callisto', 'Cerberus', 'Chambers of Xeric', 'Chambers of Xeric: Challenge Mode',
    'Chaos Elemental', 'Chaos Fanatic', 'Commander Zilyana', 'Corporeal Beast', 'Crazy Archaeologist',
    'Dagannoth Prime', 'Dagannoth Rex', 'Dagannoth Supreme', 'Deranged Archaeologist', 'General Graardor',
    'Giant Mole', 'Grotesque Guardians', 'Hespori', 'Kalphite Queen', 'King Black Dragon', 'Kraken',
    """Kree'Arra""", """K'ril Tsutsaroth""", 'Mimic', 'Nightmare', """Phosani's Nightmare""", 'Obor', 'Sarachnis',
    'Scorpia', 'Skotizo', 'Tempoross', 'The Gauntlet', 'The Corrupted Gauntlet', 'Theatre of Blood',
    'Theatre of Blood: Hard Mode', 'Thermonuclear Smoke Devil', 'TzKal-Zuk', 'TzTok-Jad', 'Venenatis',
    """Vet'ion""", 'Vorkath', 'Wintertodt', 'Zalcano', 'Zulrah'
]