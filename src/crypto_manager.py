from pycoingecko import CoinGeckoAPI

class CryptoManager:
    def __init__(self):
        self.cg = CoinGeckoAPI()
        self.coins = ["bitcoin", "ethereum", "monero", "cardano", "dogecoin"]

    def get_live_crypto_prices(self):
        prices = {}
        for coin in self.coins:
            data = self.cg.get_coin_by_id(coin)
            current_price = data["market_data"]["current_price"]["usd"]
            prices[coin] = current_price
        return prices

    def display_crypto_prices(self, prices):
        message = "**Live Cryptocurrency Prices:**\n"
        for coin, price in prices.items():
            message += f"{coin.upper()}: ${price:.2f}\n"
        return message